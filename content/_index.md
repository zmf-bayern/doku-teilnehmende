---
title: "Dokumentation"
slug: "doku"
chapter: true
date: 2021-10-05
weight: 1
---

### Dokumentation und Leitfaden des ZMF Learning Hubs aus Sicht der Teilnehmenden.

# Lassen Sie uns anfangen

Diese Dokumentation beschreibt aus Sicht der Teilnehmenden, wie an einem Kurs auf dem Learning Hub teilgenommen werden kann. Jedes Kapitel wird als handlungsorientierter Leitfaden beschrieben und mit good practise Beispielen dargestellt.

