---
title: "Themengruppe"
slug: "themengruppe"
chapter: true
date: 2021-10-05
weight: 1
comments: false
pre: "1.) "
---

### ZMF Learning Hub - hier wird gelernt

# Benutzungshandbuch

Willkommen beim Zentrum für Medienkompetenz in der Frühpädagogik. Hier lernen Sie alle Möglichkeiten des ZMF Learning Hubs kennen. Diese Dokumentation ist besonders für Teilnehmende des StMAS und Kooperationspartnern des ZMF interessant.
